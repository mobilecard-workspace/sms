package com.addcel.sms;

import com.addcel.sms.config.Configurations;
import com.addcel.sms.model.SmsConfigParam;
import com.addcel.sms.repository.SmsConfigParamRepository;
import com.addcel.sms.util.ConstantSMS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Optional;

@SpringBootApplication
public class SmsApplication extends SpringBootServletInitializer {

    @Autowired
    private SmsConfigParamRepository smsConfigParamRepository;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SmsApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(SmsApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /**
     * Beans de configuraciones pre-cargadas en el contexto de Sring
     */
    @Bean
    @PostConstruct
    public Configurations configurations() {
        Configurations configurations = new Configurations();
        Optional<SmsConfigParam> username = smsConfigParamRepository.findByKeyParam(ConstantSMS.USERNAME_CLICKSEND);
        username.ifPresent(userParam -> configurations.setUsername(userParam.getValueParam()));

        Optional<SmsConfigParam> password = smsConfigParamRepository.findByKeyParam(ConstantSMS.PASSWORD_CLICKSEND);
        password.ifPresent(passwordParam -> configurations.setPassword(passwordParam.getValueParam()));

        Optional<SmsConfigParam> urlClickSend = smsConfigParamRepository.findByKeyParam(ConstantSMS.URL_CLICKSEND);
        urlClickSend.ifPresent(urlClickSendParam -> configurations.setUrlClickSend(urlClickSendParam.getValueParam()));

        Optional<SmsConfigParam> accountSidTwilio = smsConfigParamRepository.findByKeyParam(ConstantSMS.ACCOUNT_SID_TWILIO);
        accountSidTwilio.ifPresent(accountSidTwilioParam -> configurations.setAccountSidTwilio(accountSidTwilioParam.getValueParam()));

        Optional<SmsConfigParam> authTokenTwilio = smsConfigParamRepository.findByKeyParam(ConstantSMS.AUTH_TOKEN_TWILIO);
        authTokenTwilio.ifPresent(authTokenTwilioParam -> configurations.setAuthTokenTwilio(authTokenTwilioParam.getValueParam()));

        return configurations;
    }
}
