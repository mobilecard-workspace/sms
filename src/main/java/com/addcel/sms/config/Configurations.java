package com.addcel.sms.config;

public class Configurations {
    private String username;
    private String password;
    private String urlClickSend;
    private String accountSidTwilio;
    private String authTokenTwilio;

    public String getUrlClickSend() {
        return urlClickSend;
    }

    public void setUrlClickSend(String urlClickSend) {
        this.urlClickSend = urlClickSend;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccountSidTwilio() {
        return accountSidTwilio;
    }

    public void setAccountSidTwilio(String accountSidTwilio) {
        this.accountSidTwilio = accountSidTwilio;
    }

    public String getAuthTokenTwilio() {
        return authTokenTwilio;
    }

    public void setAuthTokenTwilio(String authTokenTwilio) {
        this.authTokenTwilio = authTokenTwilio;
    }
}
