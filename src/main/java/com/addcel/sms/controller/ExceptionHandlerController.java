package com.addcel.sms.controller;

import com.addcel.sms.exception.ClientRestException;
import com.addcel.sms.response.ResponseSMS;
import com.addcel.sms.util.ConstantSMS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController {

    Logger logger = LoggerFactory.getLogger(ExceptionHandlerController.class);

    /**
     * Manejo de error en el cliente Rest de ClickSend
     *
     * @param clientRestException Exception en el cliente REST
     */
    @ExceptionHandler(ClientRestException.class)
    public ResponseEntity<ResponseSMS> clientException(ClientRestException clientRestException) {
        logger.error("Manejando error ClientRestException...", clientRestException);
        ResponseSMS responseSMS = new ResponseSMS();
        responseSMS.setCode(ConstantSMS.CODE_ERROR);
        responseSMS.setMessage(ConstantSMS.MENSAJE_ERROR);
        responseSMS.setData(clientRestException.getMessage());

        return new ResponseEntity<>(responseSMS, HttpStatus.BAD_REQUEST);
    }

    /**
     * Manejo de error en el cliente Rest de ClickSend
     *
     * @param runtimeException Exception en tiempo de ejecucion
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ResponseSMS> genericException(RuntimeException runtimeException) {
        logger.error("Manejando error ClientRestException...", runtimeException);
        ResponseSMS responseSMS = new ResponseSMS();
        responseSMS.setCode(ConstantSMS.CODE_ERROR);
        responseSMS.setMessage(ConstantSMS.MENSAJE_ERROR);
        responseSMS.setData(ConstantSMS.MESSAGE_GENERIC_ERROR);
        return new ResponseEntity<>(responseSMS, HttpStatus.BAD_REQUEST);
    }
}
