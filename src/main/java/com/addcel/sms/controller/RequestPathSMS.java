package com.addcel.sms.controller;

public interface RequestPathSMS {
    String INIT_PATH = "{idApp}/{idPais}/{idioma}";
    String SEND_PATH = "/send";
    String SEND_ACTIVATION_CODE_PATH = "/activation/code";
    String ACTIVATION_CODE_PATH = "/activate/code";
}
