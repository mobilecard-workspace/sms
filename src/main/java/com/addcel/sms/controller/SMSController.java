package com.addcel.sms.controller;

import com.addcel.sms.dto.ActivationCodeDTO;
import com.addcel.sms.dto.ActivationSendCodeDTO;
import com.addcel.sms.dto.SmsDto;
import com.addcel.sms.exception.ClientRestException;
import com.addcel.sms.exception.ServiceSmsException;
import com.addcel.sms.request.ActivationCodeRequest;
import com.addcel.sms.request.ActivationSmsCodeRequest;
import com.addcel.sms.request.SmsRequest;
import com.addcel.sms.response.ResponseSMS;
import com.addcel.sms.service.SmsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SMSController {

    Logger logger = LoggerFactory.getLogger(SMSController.class);
    @Autowired
    private SmsService SMSService;

    /**
     * Endpoint API SMS
     */
    @RequestMapping(value = RequestPathSMS.INIT_PATH + RequestPathSMS.SEND_PATH, method = RequestMethod.POST)
    @ResponseBody
    public ResponseSMS sendSMS(
            @PathVariable("idApp") Integer idApp,
            @PathVariable("idPais") String idPais,
            @PathVariable("idioma") String idioma,
            @RequestBody SmsRequest smsRequest
    ) throws ClientRestException {
        logger.info("|---> inicia peticion al endpoint sms/send");
        logger.info("parametros: " + smsRequest.toString());
        SmsDto smsDto = new SmsDto();
        smsDto.setIdApp(idApp);
        smsDto.setIdioma(idioma);
        smsDto.setIdPais(idPais);
        smsDto.setSmsRequest(smsRequest);

        return SMSService.sendSMSClickSend(smsDto);
    }

    /**
     * Genera un codigo de activacion y lo envia por SMS
     */
    @PostMapping(value = RequestPathSMS.INIT_PATH + RequestPathSMS.SEND_ACTIVATION_CODE_PATH)
    @ResponseBody
    public ResponseEntity sendActivationCode(
            @PathVariable("idApp") Integer idApp,
            @PathVariable("idPais") String idPais,
            @PathVariable("idioma") String idioma,
            @RequestBody ActivationSmsCodeRequest request
    ) {
        ActivationSendCodeDTO activationSendCodeDTO = new ActivationSendCodeDTO();
        activationSendCodeDTO.setIdApp(idApp);
        activationSendCodeDTO.setIdioma(idioma);
        activationSendCodeDTO.setIdPais(idPais);
        activationSendCodeDTO.setActivationSmsCodeRequest(request);
        logger.info("Request DTO: " + activationSendCodeDTO.toString());

        try {
            ResponseSMS responseSMS = SMSService.sendActivationCode(activationSendCodeDTO);
            return new ResponseEntity(responseSMS, HttpStatus.OK);
        } catch (ServiceSmsException e) {
            return new ResponseEntity(new ResponseSMS(e.getCode(), e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Genera un codigo de activacion y lo envia por SMS
     */
    @PostMapping(value = RequestPathSMS.INIT_PATH + RequestPathSMS.ACTIVATION_CODE_PATH )
    @ResponseBody
    public ResponseEntity activationCode(
            @PathVariable("idApp") Integer idApp,
            @PathVariable("idPais") String idPais,
            @PathVariable("idioma") String idioma,
            @RequestBody ActivationCodeRequest request
    ) {
        ActivationCodeDTO activationSendCodeDTO = new ActivationCodeDTO();
        activationSendCodeDTO.setIdApp(idApp);
        activationSendCodeDTO.setIdioma(idioma);
        activationSendCodeDTO.setIdPais(idPais);
        activationSendCodeDTO.setActivationCodeRequest(request);
        logger.info("Request DTO: " + activationSendCodeDTO.toString());

        try {
            ResponseSMS responseSMS = SMSService.activeCode(activationSendCodeDTO);
            return new ResponseEntity(responseSMS, HttpStatus.OK);
        } catch (ServiceSmsException e) {
            return new ResponseEntity(new ResponseSMS(e.getCode(), e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
}
