package com.addcel.sms.dto;

import com.addcel.sms.request.ActivationCodeRequest;

public class ActivationCodeDTO extends RequestDTO{
    private ActivationCodeRequest activationCodeRequest;

    @Override
    public String toString() {
        return "ActivationCodeDTO{" +
                "activationCodeRequest=" + activationCodeRequest +
                '}';
    }

    public ActivationCodeRequest getActivationCodeRequest() {
        return activationCodeRequest;
    }

    public void setActivationCodeRequest(ActivationCodeRequest activationCodeRequest) {
        this.activationCodeRequest = activationCodeRequest;
    }
}
