package com.addcel.sms.dto;

import com.addcel.sms.request.ActivationSmsCodeRequest;

public class ActivationSendCodeDTO extends RequestDTO{
    private ActivationSmsCodeRequest activationSmsCodeRequest;

    public ActivationSmsCodeRequest getActivationSmsCodeRequest() {
        return activationSmsCodeRequest;
    }

    public void setActivationSmsCodeRequest(ActivationSmsCodeRequest activationSmsCodeRequest) {
        this.activationSmsCodeRequest = activationSmsCodeRequest;
    }

    @Override
    public String toString() {
        return "ActivationSendCodeDTO{" +
                "activationSmsCodeRequest=" + activationSmsCodeRequest +
                '}';
    }
}
