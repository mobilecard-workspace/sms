package com.addcel.sms.dto;

public abstract class RequestDTO {
    private Integer idApp;
    private String idPais;
    private String idioma;

    public Integer getIdApp() {
        return idApp;
    }

    public void setIdApp(Integer idApp) {
        this.idApp = idApp;
    }

    public String getIdPais() {
        return idPais;
    }

    public void setIdPais(String idPais) {
        this.idPais = idPais;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    @Override
    public String toString() {
        return "RequestDTO{" +
                "idApp=" + idApp +
                ", idPais='" + idPais + '\'' +
                ", idioma='" + idioma + '\'' +
                '}';
    }
}
