package com.addcel.sms.dto;

import com.addcel.sms.request.SmsRequest;

public class SmsDto {
    private SmsRequest smsRequest;
    private Integer idApp;
    private String idPais;
    private String idioma;

    public SmsRequest getSmsRequest() {
        return smsRequest;
    }

    public void setSmsRequest(SmsRequest smsRequest) {
        this.smsRequest = smsRequest;
    }

    public Integer getIdApp() {
        return idApp;
    }

    public void setIdApp(Integer idApp) {
        this.idApp = idApp;
    }

    public String getIdPais() {
        return idPais;
    }

    public void setIdPais(String idPais) {
        this.idPais = idPais;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    @Override
    public String toString() {
        return "SmsDto{" +
                "smsRequest=" + smsRequest +
                ", idApp=" + idApp +
                ", idPais='" + idPais + '\'' +
                ", idioma='" + idioma + '\'' +
                '}';
    }
}
