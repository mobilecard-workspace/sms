package com.addcel.sms.exception;

public class ClientRestException extends Exception{
    public static final String ERROR_CLIENT = "Ocurrio un error al enviar la solicitud.";
    public static final String ERROR_SERVER = "Ocurrio un error en el servicio de envio.";
    public static final String ERROR_RESOURCE_ACCES = "Servicio no disponible.";

    public ClientRestException(String message, Throwable cause) {
        super(message, cause);
    }
}
