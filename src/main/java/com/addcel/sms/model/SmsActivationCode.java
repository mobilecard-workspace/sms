package com.addcel.sms.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "SMS_ACTIVATION_CODE")
public class SmsActivationCode {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "ID_ACTIVATION_CODE")
    private Integer idSmsActivationCode;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "IMEI")
    private String imei;

    @Column(name = "CODE")
    private String code;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private StatusActivationCode status;

    @Column(name = "DATE", columnDefinition="DATETIME")
    private Timestamp date;

    public Integer getIdSmsActivationCode() {
        return idSmsActivationCode;
    }

    public void setIdSmsActivationCode(Integer idSmsActivationCode) {
        this.idSmsActivationCode = idSmsActivationCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public StatusActivationCode getStatus() {
        return status;
    }

    public void setStatus(StatusActivationCode status) {
        this.status = status;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}
