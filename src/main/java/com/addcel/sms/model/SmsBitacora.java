package com.addcel.sms.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SMS_BITACORA")
public class SmsBitacora {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_BITACORA")
    private Integer idBitacora;
    @Column(name = "FECHA")
    private Date fecha;
    @Column(name = "REQUEST_ENDPOINT")
    private String requestEndpoint;
    @Column(name = "RESPONSE_ENDPOINT")
    private String responseEndpoint;
    @Column(name = "REQUEST_CLICKSEND")
    private String requestClickSend;
    @Column(name = "RESPONSE_CLICKSEND")
    private String responseClickSend;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "ID_APP")
    private Integer idApp;
    @Column(name = "ID_PAIS")
    private String idPais;
    @Column(name = "IDIOMA")
    private String idioma;

    public Integer getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(Integer idBitacora) {
        this.idBitacora = idBitacora;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRequestEndpoint() {
        return requestEndpoint;
    }

    public void setRequestEndpoint(String requestEndpoint) {
        this.requestEndpoint = requestEndpoint;
    }

    public String getResponseEndpoint() {
        return responseEndpoint;
    }

    public void setResponseEndpoint(String responseEndpoint) {
        this.responseEndpoint = responseEndpoint;
    }

    public String getRequestClickSend() {
        return requestClickSend;
    }

    public void setRequestClickSend(String requestClickSend) {
        this.requestClickSend = requestClickSend;
    }

    public String getResponseClickSend() {
        return responseClickSend;
    }

    public void setResponseClickSend(String responseClickSend) {
        this.responseClickSend = responseClickSend;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdApp() {
        return idApp;
    }

    public void setIdApp(Integer idApp) {
        this.idApp = idApp;
    }

    public String getIdPais() {
        return idPais;
    }

    public void setIdPais(String idPais) {
        this.idPais = idPais;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }
}
