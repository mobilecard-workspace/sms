package com.addcel.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SMS_CONFIG_PARAM")
public class SmsConfigParam {
    @Id
    @Column(name = "ID_CONFIG_PARAM")
    private Integer idConfigParamM;
    @Column(name = "KEY_PARAM")
    private String keyParam;
    @Column(name = "VALUE_PARAM")
    private String valueParam;

    public Integer getIdConfigParamM() {
        return idConfigParamM;
    }

    public void setIdConfigParamM(Integer idConfigParamM) {
        this.idConfigParamM = idConfigParamM;
    }

    public String getKeyParam() {
        return keyParam;
    }

    public void setKeyParam(String keyParam) {
        this.keyParam = keyParam;
    }

    public String getValueParam() {
        return valueParam;
    }

    public void setValueParam(String valueParam) {
        this.valueParam = valueParam;
    }
}
