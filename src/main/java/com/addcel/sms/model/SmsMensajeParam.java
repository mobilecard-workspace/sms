package com.addcel.sms.model;

import javax.persistence.*;

@Entity
@Table(name = "SMS_MENSAJE_PARAMS")
public class SmsMensajeParam {
    @Id
    @Column(name = "ID_MENSAJE_PARAMS")
    private Integer idMensajeParams;
    @Column(name = "KEY_PARAM")
    private String keyParam;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_MENSAJE")
    private SmsMensajes smsMensajes;

    public Integer getIdMensajeParams() {
        return idMensajeParams;
    }

    public void setIdMensajeParams(Integer idMensajeParams) {
        this.idMensajeParams = idMensajeParams;
    }

    public String getKeyParam() {
        return keyParam;
    }

    public void setKeyParam(String keyParam) {
        this.keyParam = keyParam;
    }

    public SmsMensajes getSmsMensajes() {
        return smsMensajes;
    }

    public void setSmsMensajes(SmsMensajes smsMensajes) {
        this.smsMensajes = smsMensajes;
    }

    @Override
    public String toString() {
        return "SmsMensajeParam{" +
                "idMensajeParams=" + idMensajeParams +
                ", keyParam='" + keyParam + '\'' +
                '}';
    }
}
