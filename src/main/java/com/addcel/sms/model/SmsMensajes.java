package com.addcel.sms.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "SMS_MENSAJES")
public class SmsMensajes {
    @Id
    @Column(name = "ID_MENSAJE")
    private Integer idMensaje;
    @Column(name = "MENSAJE")
    private String mensaje;
    @Column(name = "IDIOMA")
    private String idioma;
    @OneToMany(mappedBy = "smsMensajes")
    private List<SmsMensajeParam> smsMensajeParams;

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public List<SmsMensajeParam> getSmsMensajeParams() {
        return smsMensajeParams;
    }

    public void setSmsMensajeParams(List<SmsMensajeParam> smsMensajeParams) {
        this.smsMensajeParams = smsMensajeParams;
    }

    public Integer getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(Integer idMensaje) {
        this.idMensaje = idMensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "SmsMensajes{" +
                "idMensaje=" + idMensaje +
                ", mensaje='" + mensaje + '\'' +
                ", idioma='" + idioma + '\'' +
                ", smsMensajeParams=" + smsMensajeParams +
                '}';
    }
}
