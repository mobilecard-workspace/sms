package com.addcel.sms.repository;

import com.addcel.sms.model.SmsActivationCode;
import com.addcel.sms.model.StatusActivationCode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Repository
public interface SmsActivationCodeRepository extends CrudRepository<SmsActivationCode, Integer> {

    /**
     * Buscar los codigo generados
     *
     * @param imei     IMEI codigo unico de dispositivo
     * @param dateFrom fecha fin
     * @param dateTo   fecha inicio
     */
    Optional<List<SmsActivationCode>> findByImeiAndDateBetween(String imei, Timestamp dateFrom, Timestamp dateTo);

    /**
     * Obtener codigos por IMEI
     *
     * @param imei   IMEI
     * @param status Status del codigo
     */
    Optional<List<SmsActivationCode>> findByImeiAndStatus(String imei, StatusActivationCode status);

    /**
     * Obtener codigos por phone numer
     *
     * @param phoneNumber IMEI
     * @param status      Status del codigo
     */
    Optional<List<SmsActivationCode>> findByPhoneNumberAndStatus(String phoneNumber, StatusActivationCode status);

    /**
     * Buscar por codigo
     */
    Optional<SmsActivationCode> findByCodeAndPhoneNumberAndImei(String code, String phoneNumber, String imei);
}
