package com.addcel.sms.repository;

import com.addcel.sms.model.SmsBitacora;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsBitacoraRepository extends CrudRepository<SmsBitacora, Integer> {
}
