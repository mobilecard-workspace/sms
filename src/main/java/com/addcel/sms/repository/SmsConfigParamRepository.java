package com.addcel.sms.repository;

import com.addcel.sms.model.SmsConfigParam;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SmsConfigParamRepository extends CrudRepository<SmsConfigParam, Integer> {

    /**
     * Buscar por key value
     *
     * @param keyParam Llave de la configuracion a buscar
     */
    Optional<SmsConfigParam> findByKeyParam(String keyParam);
}
