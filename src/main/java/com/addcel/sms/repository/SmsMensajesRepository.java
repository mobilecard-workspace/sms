package com.addcel.sms.repository;

import com.addcel.sms.model.SmsMensajes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SmsMensajesRepository extends CrudRepository<SmsMensajes, Integer> {

    /**
     * Buscar por id de mensaje e idioma
     */
    Optional<SmsMensajes> findByIdMensajeAndIdioma(Integer idMensaje, String idioma);
}
