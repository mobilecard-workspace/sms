package com.addcel.sms.repository;

import com.addcel.sms.model.TUsuarios;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Repository
public interface TUsuarioRepository extends CrudRepository<TUsuarios, BigInteger> {

    /**
     * Buscar por numero de telefono
     */
    Optional<List<TUsuarios>> findByUsrTelefono(String usrTelefono);
}
