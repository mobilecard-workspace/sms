package com.addcel.sms.request;

public class ActivationSmsCodeRequest {
    private String phoneNumber;
    private String imei;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    @Override
    public String toString() {
        return "ActivationSmsCodeRequest{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", imei='" + imei + '\'' +
                '}';
    }
}
