package com.addcel.sms.request;

import java.util.ArrayList;
import java.util.List;

public class ClickSendRequest {
    private List<MessagesClickSend> messages;

    public ClickSendRequest() {
        this.messages = new ArrayList<>();
    }

    public List<MessagesClickSend> getMessages() {
        return messages;
    }

    public void setMessages(List<MessagesClickSend> messages) {
        this.messages = messages;
    }

    public void addMessage(MessagesClickSend messagesClickSend) {
        this.messages.add(messagesClickSend);
    }

    @Override
    public String toString() {
        return "ClickSendRequest{" +
                "messages=" + messages +
                '}';
    }
}
