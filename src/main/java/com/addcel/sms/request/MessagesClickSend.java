package com.addcel.sms.request;

public class MessagesClickSend {
    private String source;
    private String from;
    private String body;
    private String to;
    private String schedule;
    private String customString;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getCustomString() {
        return customString;
    }

    public void setCustomString(String customString) {
        this.customString = customString;
    }

    @Override
    public String toString() {
        return "MessagesClickSend{" +
                "source='" + source + '\'' +
                ", from='" + from + '\'' +
                ", body='" + body + '\'' +
                ", to='" + to + '\'' +
                ", schedule='" + schedule + '\'' +
                ", customString='" + customString + '\'' +
                '}';
    }
}
