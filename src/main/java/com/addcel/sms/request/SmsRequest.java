package com.addcel.sms.request;

import java.util.Map;

public class SmsRequest {
    private String idUsuario;
    private Integer idMensaje;
    private String numeroCelular;
    private Map<String, String> params;

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(Integer idMensaje) {
        this.idMensaje = idMensaje;
    }

    public String getNumeroCelular() {
        return numeroCelular;
    }

    public void setNumeroCelular(String numeroCelular) {
        this.numeroCelular = numeroCelular;
    }

    @Override
    public String toString() {
        return "SmsRequest{" +
                "idUsuario='" + idUsuario + '\'' +
                ", idMensaje=" + idMensaje +
                ", numeroCelular='" + numeroCelular + '\'' +
                ", params=" + params +
                '}';
    }
}