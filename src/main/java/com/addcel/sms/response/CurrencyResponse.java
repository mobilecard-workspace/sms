package com.addcel.sms.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CurrencyResponse {
    @JsonProperty("currency_name_short")
    private String currencyNameShort;
    @JsonProperty("currency_prefix_d")
    private String currencyPrefixD;
    @JsonProperty("currency_prefix_c")
    private String currencyPrefixC;
    @JsonProperty("currency_name_long")
    private String currencyNameLong;

    public String getCurrencyNameShort() {
        return currencyNameShort;
    }

    public void setCurrencyNameShort(String currencyNameShort) {
        this.currencyNameShort = currencyNameShort;
    }

    public String getCurrencyPrefixD() {
        return currencyPrefixD;
    }

    public void setCurrencyPrefixD(String currencyPrefixD) {
        this.currencyPrefixD = currencyPrefixD;
    }

    public String getCurrencyPrefixC() {
        return currencyPrefixC;
    }

    public void setCurrencyPrefixC(String currencyPrefixC) {
        this.currencyPrefixC = currencyPrefixC;
    }

    public String getCurrencyNameLong() {
        return currencyNameLong;
    }

    public void setCurrencyNameLong(String currencyNameLong) {
        this.currencyNameLong = currencyNameLong;
    }

    @Override
    public String toString() {
        return "CurrencyResponse{" +
                "currencyNameShort='" + currencyNameShort + '\'' +
                ", currencyPrefixD='" + currencyPrefixD + '\'' +
                ", currencyPrefixC='" + currencyPrefixC + '\'' +
                ", currencyNameLong='" + currencyNameLong + '\'' +
                '}';
    }
}
