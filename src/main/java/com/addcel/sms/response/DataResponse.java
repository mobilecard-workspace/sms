package com.addcel.sms.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class DataResponse {
    @JsonProperty("total_price")
    private Double totalPrice;
    @JsonProperty("total_count")
    private Double totalCount;
    @JsonProperty("queued_count")
    private Double queuedCount;
    @JsonProperty("messages")
    private List<MessagesResponse> messagesResponses;
    @JsonProperty("_currency")
    private CurrencyResponse currencyResponse;

    public CurrencyResponse getCurrencyResponse() {
        return currencyResponse;
    }

    public void setCurrencyResponse(CurrencyResponse currencyResponse) {
        this.currencyResponse = currencyResponse;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Double totalCount) {
        this.totalCount = totalCount;
    }

    public Double getQueuedCount() {
        return queuedCount;
    }

    public void setQueuedCount(Double queuedCount) {
        this.queuedCount = queuedCount;
    }

    public List<MessagesResponse> getMessagesResponses() {
        return messagesResponses;
    }

    public void setMessagesResponses(List<MessagesResponse> messagesResponses) {
        this.messagesResponses = messagesResponses;
    }

    @Override
    public String toString() {
        return "DataResponse{" +
                "totalPrice=" + totalPrice +
                ", totalCount=" + totalCount +
                ", queuedCount=" + queuedCount +
                ", messagesResponses=" + messagesResponses +
                ", currencyResponse=" + currencyResponse +
                '}';
    }
}
