package com.addcel.sms.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MessagesResponse {
    @JsonProperty("direction")
    private String direction;
    @JsonProperty("date")
    private Integer date;
    @JsonProperty("to")
    private String to;
    @JsonProperty("body")
    private String body;
    @JsonProperty("from")
    private String from;
    @JsonProperty("schedule")
    private Integer schedule;
    @JsonProperty("message_id")
    private String messageId;
    @JsonProperty("message_parts")
    private Integer messageParts;
    @JsonProperty("message_price")
    private String messagePrice;
    @JsonProperty("from_email")
    private String fromEmail;
    @JsonProperty("list_id")
    private String listId;
    @JsonProperty("custom_string")
    private String customString;
    @JsonProperty("contact_id")
    private String contactId;
    @JsonProperty("user_id")
    private Integer userId;
    @JsonProperty("subaccount_id")
    private Integer subaccountId;
    @JsonProperty("country")
    private String country;
    @JsonProperty("carrier")
    private String carrier;
    @JsonProperty("status")
    private String status;

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Integer getSchedule() {
        return schedule;
    }

    public void setSchedule(Integer schedule) {
        this.schedule = schedule;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Integer getMessageParts() {
        return messageParts;
    }

    public void setMessageParts(Integer messageParts) {
        this.messageParts = messageParts;
    }

    public String getMessagePrice() {
        return messagePrice;
    }

    public void setMessagePrice(String messagePrice) {
        this.messagePrice = messagePrice;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getCustomString() {
        return customString;
    }

    public void setCustomString(String customString) {
        this.customString = customString;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSubaccountId() {
        return subaccountId;
    }

    public void setSubaccountId(Integer subaccountId) {
        this.subaccountId = subaccountId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "MessagesResponse{" +
                "direction='" + direction + '\'' +
                ", date=" + date +
                ", to='" + to + '\'' +
                ", body='" + body + '\'' +
                ", from='" + from + '\'' +
                ", schedule=" + schedule +
                ", messageId='" + messageId + '\'' +
                ", messageParts=" + messageParts +
                ", messagePrice='" + messagePrice + '\'' +
                ", fromEmail='" + fromEmail + '\'' +
                ", listId='" + listId + '\'' +
                ", customString='" + customString + '\'' +
                ", contactId='" + contactId + '\'' +
                ", userId=" + userId +
                ", subaccountId=" + subaccountId +
                ", country='" + country + '\'' +
                ", carrier='" + carrier + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
