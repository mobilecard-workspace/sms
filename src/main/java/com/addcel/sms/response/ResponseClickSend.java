package com.addcel.sms.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseClickSend {
    @JsonProperty("http_code")
    private String httpCode;
    @JsonProperty("response_code")
    private String responseCode;
    @JsonProperty("response_msg")
    private String responseMsg;
    @JsonProperty("data")
    private DataResponse data;

    public String getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(String httpCode) {
        this.httpCode = httpCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public DataResponse getData() {
        return data;
    }

    public void setData(DataResponse data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseClickSend{" +
                "httpCode='" + httpCode + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", responseMsg='" + responseMsg + '\'' +
                ", data=" + data +
                '}';
    }
}
