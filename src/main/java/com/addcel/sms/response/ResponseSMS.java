package com.addcel.sms.response;

import com.addcel.sms.util.ConstantSMS;

public class ResponseSMS {
    private int code;
    private String message;
    private Object data;

    public ResponseSMS() {
        this.code = 1;
        this.message = ConstantSMS.MENSAJE_EXITO;
    }

    public ResponseSMS(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseSMS{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
