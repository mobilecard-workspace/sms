package com.addcel.sms.service;

import com.addcel.sms.config.Configurations;
import com.addcel.sms.exception.ClientRestException;
import com.addcel.sms.request.ClickSendRequest;
import com.addcel.sms.response.ResponseClickSend;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Service
public class ClickSendClientRest {
    Logger logger = LoggerFactory.getLogger(ClickSendClientRest.class);
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private Configurations configurations;


    /**
     * Enviar peticion REST al API de ClickSend
     */
    public ResponseClickSend sendSmsRest(ClickSendRequest clickSendRequest, String authorizationHeader) throws ClientRestException {
        logger.info("exce Client REST sendSmsRest");
        logger.info("JSON body: " + clickSendRequest.toString());
        logger.info("Header Authorization: " + authorizationHeader);

        try {
            String url = configurations.getUrlClickSend();
            logger.info("URL ClickSend: " + url);
            MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
            headers.add("Authorization", "Basic: " + authorizationHeader);
            headers.add("Content-Type", "application/json");
            HttpEntity<ClickSendRequest> request = new HttpEntity<>(clickSendRequest, headers);

            ResponseEntity<ResponseClickSend> response = restTemplate.postForEntity(url, request, ResponseClickSend.class);
            logger.info("Response: " + response);

            return response.getBody();
        } catch (HttpClientErrorException httpClientError) {
            logger.error("La solicitud contiene sintaxis incorrecta o no puede procesarse.", httpClientError);
            logger.error("HTTP Status: " + httpClientError.getStatusCode().getReasonPhrase());
            throw new ClientRestException(ClientRestException.ERROR_CLIENT, httpClientError);
        } catch (HttpServerErrorException httpServerError) {
            logger.error("El servidor falló al completar una solicitud aparentemente válida.", httpServerError);
            logger.error("HTTP Status: " + httpServerError.getStatusCode().getReasonPhrase());
            throw new ClientRestException(ClientRestException.ERROR_SERVER, httpServerError);
        } catch (ResourceAccessException resourceAccessException) {
            logger.error("El recurso del API de ClickSend no esta disponible", resourceAccessException);
            logger.error("Mensaje" + resourceAccessException.getMessage());
            throw new ClientRestException(ClientRestException.ERROR_RESOURCE_ACCES, resourceAccessException);
        }
    }
}
