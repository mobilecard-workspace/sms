package com.addcel.sms.service;

import com.addcel.sms.dto.ActivationCodeDTO;
import com.addcel.sms.dto.ActivationSendCodeDTO;
import com.addcel.sms.dto.SmsDto;
import com.addcel.sms.exception.ClientRestException;
import com.addcel.sms.exception.ServiceSmsException;
import com.addcel.sms.response.ResponseSMS;

public interface SmsService {

    /**
     * Enviar sms a traves del api de ClickSend
     *
     * @param smsDto DTO del controler al servicio
     */
    ResponseSMS sendSMSClickSend(SmsDto smsDto) throws ClientRestException;

    /**
     * Generar el codigo de activasion y enviarlo por sms
     *
     * @param activationSendCodeDTO DTO con el request de entrada al servicio
     */
    ResponseSMS sendActivationCode(ActivationSendCodeDTO activationSendCodeDTO) throws ServiceSmsException;

    /**
     * Activar codigo
     * */
    ResponseSMS activeCode(ActivationCodeDTO activationCodeDTO) throws ServiceSmsException;
}