package com.addcel.sms.service;

import com.addcel.sms.config.Configurations;
import com.addcel.sms.dto.ActivationCodeDTO;
import com.addcel.sms.dto.ActivationSendCodeDTO;
import com.addcel.sms.dto.SmsDto;
import com.addcel.sms.exception.ClientRestException;
import com.addcel.sms.exception.ServiceSmsException;
import com.addcel.sms.exception.TwilioClientException;
import com.addcel.sms.model.*;
import com.addcel.sms.repository.SmsActivationCodeRepository;
import com.addcel.sms.repository.SmsBitacoraRepository;
import com.addcel.sms.repository.SmsMensajesRepository;
import com.addcel.sms.repository.TUsuarioRepository;
import com.addcel.sms.request.ClickSendRequest;
import com.addcel.sms.request.MessagesClickSend;
import com.addcel.sms.response.ResponseSMS;
import com.addcel.sms.util.CodeNumberCountry;
import com.addcel.sms.util.ConstantSMS;
import com.addcel.sms.util.SMSUtil;
import com.twilio.rest.api.v2010.account.Message;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class SmsServiceImpl implements SmsService {

    private static Logger logger = LoggerFactory.getLogger(SmsServiceImpl.class);

    @Autowired
    private SmsMensajesRepository smsMensajesRepository;

    @Autowired
    private Configurations configurations;

    @Autowired
    private ClickSendClientRest clickSendClientRest;

    @Autowired
    private SmsBitacoraRepository smsBitacoraRepository;

    @Autowired
    private TwilioClientService twilioClientService;

    @Autowired
    private SmsActivationCodeRepository smsActivationCodeRepository;

    @Autowired
    private TUsuarioRepository tUsuarioRepository;

    private final Integer ID_MENSAJE_ACTIVE_CODE = 3;

    private final String PARAM_SMS_CODE = "<nip>";

    private final Integer CODE_EXITO = 0;

    /**
     * Enviar sms a traves del api de ClickSend
     *
     * @param smsDto DTO del controler al servicio
     */
    @Override
    public ResponseSMS sendSMSClickSend(SmsDto smsDto) throws ClientRestException {
        logger.info("|---> exec service sendSMSClickSend()");
        ResponseSMS responseSMS = new ResponseSMS();

        //guardar en bitacora
        SmsBitacora smsBitacora = new SmsBitacora();
        smsBitacora.setFecha(new Date());
        smsBitacora.setRequestEndpoint(smsDto.toString());
        smsBitacora.setDescripcion("Enviando mensaje ID: " + smsDto.getSmsRequest().getIdMensaje());
        smsBitacora.setIdApp(smsDto.getIdApp());
        smsBitacora.setIdPais(smsDto.getIdPais());
        smsBitacora.setIdioma(smsDto.getIdioma());

        ClickSendRequest request = new ClickSendRequest();
        MessagesClickSend messagesClickSend = new MessagesClickSend();

        //JSON Body
        messagesClickSend.setBody(getMessageById(smsDto.getSmsRequest().getIdMensaje(), smsDto.getSmsRequest().getParams(), smsDto.getIdioma()));
        messagesClickSend.setCustomString(ConstantSMS.REFERENCE_SMS);
        messagesClickSend.setSource(ConstantSMS.SOURCE);
        messagesClickSend.setFrom(ConstantSMS.FROM);
        messagesClickSend.setSchedule(String.valueOf(SMSUtil.getUnixTimestamp()));
        messagesClickSend.setTo(smsDto.getSmsRequest().getNumeroCelular());
        request.addMessage(messagesClickSend);

        smsBitacora.setRequestEndpoint(request.toString());

        Message response = null;
        try {
            response = twilioClientService.sendSmsTwilio(messagesClickSend.getBody(), smsDto.getSmsRequest().getNumeroCelular());
        } catch (TwilioClientException e) {
            logger.error("Ocurrio un error al enviar SMS API Twilio.", e);
        }

        smsBitacora.setResponseClickSend(response.toString());
        saveBitacoraSms(smsBitacora);

        return responseSMS;
    }

    /**
     * Generar el codigo de activacion y enviarlo por sms
     *
     * @param activationSendCodeDTO DTO con el request de entrada al servicio
     */
    @Override
    public ResponseSMS sendActivationCode(ActivationSendCodeDTO activationSendCodeDTO) throws ServiceSmsException {
        logger.info("exec sendActivationCode, dto: " + activationSendCodeDTO.toString());
        ResponseSMS response = new ResponseSMS();

        if (isCodeActiveForImei(activationSendCodeDTO.getActivationSmsCodeRequest().getImei())) {
            throw new ServiceSmsException(ConstantSMS.MESSAGE_ERROR_CODE_ACTIVE, ConstantSMS.CODE_ERROR_CODE_ACTIVE);
        }

        if (isCodeActiveForPhoneNumber(activationSendCodeDTO.getActivationSmsCodeRequest().getPhoneNumber())) {
            response.setCode(ConstantSMS.CODE_ERROR_CODE_ACTIVE_PHONE_NUMBER);
            response.setMessage(ConstantSMS.MESSAGE_ERROR_CODE_ACTIVE_PHONE_NUMBER);
            return response;
        }

        if (existNumberInTUsuarios(activationSendCodeDTO.getActivationSmsCodeRequest().getPhoneNumber())) {
            throw new ServiceSmsException(ConstantSMS.MESSAGE_ERROR_NUMBER_EXIST_TUSUARIOS, ConstantSMS.CODE_ERROR_CODE_NUMBER_EXIST_TUSUARIOS);
        }

        Optional<List<SmsActivationCode>> codeToday = getCodeTodayByImei(activationSendCodeDTO.getActivationSmsCodeRequest().getImei());
        if (codeToday.isPresent()) {
            if (codeToday.get().size() >= ConstantSMS.MAX_CODE_BY_DAY) {
                throw new ServiceSmsException(ConstantSMS.MESSAGE_ERROR_MAX_CODE_DAY, ConstantSMS.CODE_ERROR_MAX_CODE_DAY);
            }
        }
        SmsActivationCode responseData = saveCode(activationSendCodeDTO);
        Map<String, String> paramsRequest = new HashMap<>();
        paramsRequest.put(PARAM_SMS_CODE, responseData.getCode());
        String message = getMessageById(ID_MENSAJE_ACTIVE_CODE, paramsRequest, activationSendCodeDTO.getIdioma());
        try {
            String phoneNumberWithCode = CodeNumberCountry.getByCode(activationSendCodeDTO.getIdPais()).getLada() + activationSendCodeDTO.getActivationSmsCodeRequest().getPhoneNumber();
            twilioClientService.sendSmsTwilio(message, phoneNumberWithCode);
        } catch (TwilioClientException e) {
            throw new ServiceSmsException(ConstantSMS.MESSAGE_ERROR_SEND_SMS, ConstantSMS.CODE_ERROR_SEND_SMS);
        }

        response.setData(responseData);
        return response;
    }

    @Override
    public ResponseSMS activeCode(ActivationCodeDTO activationCodeDTO) throws ServiceSmsException {
        Optional<SmsActivationCode> activationCode = smsActivationCodeRepository.findByCodeAndPhoneNumberAndImei(
                activationCodeDTO.getActivationCodeRequest().getCode(),
                activationCodeDTO.getActivationCodeRequest().getPhoneNumber(),
                activationCodeDTO.getActivationCodeRequest().getImei());
        ResponseSMS response = new ResponseSMS();
        if (activationCode.isPresent()) {
            SmsActivationCode smsActivationCode = activationCode.get();
            smsActivationCode.setStatus(StatusActivationCode.ACTIVE);
            smsActivationCodeRepository.save(smsActivationCode);
            response.setData(smsActivationCode);
            response.setMessage("Codigo validado exitosamente.");
        } else {
            throw new ServiceSmsException(ConstantSMS.MESSAGE_ERROR_CODE_NOT_FOUND, ConstantSMS.CODE_ERROR_CODE_NOT_FOUND);
        }
        return response;
    }

    /**
     * Guardar cidigo por registro
     */
    private SmsActivationCode saveCode(ActivationSendCodeDTO activationSendCodeDTO) {
        logger.info("save in SMS_ACTIVATION_CODE");
        SmsActivationCode smsActivationCode = new SmsActivationCode();
        smsActivationCode.setPhoneNumber(activationSendCodeDTO.getActivationSmsCodeRequest().getPhoneNumber());
        smsActivationCode.setImei(activationSendCodeDTO.getActivationSmsCodeRequest().getImei());
        smsActivationCode.setCode(String.valueOf(RandomUtils.nextInt(100000, 999999)));
        smsActivationCode.setStatus(StatusActivationCode.INACTIVE);
        smsActivationCode.setDate(Timestamp.valueOf(LocalDateTime.now()));

        smsActivationCodeRepository.save(smsActivationCode);
        return smsActivationCode;
    }

    /**
     * Obtener los codigo generados por un IMEI en la fecha actual
     *
     * @param imei IMEI
     */
    private Optional getCodeTodayByImei(String imei) {
        logger.info("exec getCodeTodayByImei imei: {}", imei);
        LocalDateTime dateFrom = LocalDate.now().atStartOfDay();
        LocalDateTime dateTo = LocalDate.now().atStartOfDay().plusDays(1).minusSeconds(1);
        logger.info("dateFrom: " + dateFrom);
        logger.info("dateTo: " + dateTo);

        return smsActivationCodeRepository.findByImeiAndDateBetween(imei, Timestamp.valueOf(dateFrom), Timestamp.valueOf(dateTo));
    }

    /**
     * Validar si un IMEI ya tiene un codigo activo
     *
     * @param imei IMEI
     */
    private boolean isCodeActiveForImei(String imei) {
        logger.info("exec isCodeActiveForImei imei: {}", imei);
        Optional<List<SmsActivationCode>> activationCode = smsActivationCodeRepository.findByImeiAndStatus(imei, StatusActivationCode.ACTIVE);
        if (activationCode.isPresent()) {
            if (activationCode.get().size() > 0) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    private boolean existNumberInTUsuarios(String number) {
        logger.info("exec existNumberInTUsuarios, number: {}", number);
        Optional<List<TUsuarios>> tUsuarios = tUsuarioRepository.findByUsrTelefono(number);
        if (tUsuarios.isPresent()) {
            if (tUsuarios.get().size() > 0) {
                return true;
            } else return false;
        } else
            return false;
    }

    /**
     * Validar si un IMEI ya tiene un codigo activo
     *
     * @param phoneNumber IMEI
     */
    private boolean isCodeActiveForPhoneNumber(String phoneNumber) {
        logger.info("exec isCodeActiveForPhoneNumber");
        Optional<List<SmsActivationCode>> activationCode = smsActivationCodeRepository.findByPhoneNumberAndStatus(phoneNumber, StatusActivationCode.ACTIVE);
        if (activationCode.isPresent()) {
            if (activationCode.get().size() > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    /**
     * Buscar mensaje en la base de datos
     */
    private String getMessageById(Integer id, Map<String, String> paramsRequest, String idioma) {
        logger.info("exec getMessageById()");
        Optional<SmsMensajes> mensaje = smsMensajesRepository.findByIdMensajeAndIdioma(id, idioma);
        String mensajeEnd = mensaje.get().getMensaje();

        if (mensaje.isPresent()) {
            logger.info("Mensaje: " + mensaje.get().getMensaje());
            logger.info("Params replace DB: " + mensaje.get().getSmsMensajeParams().toString());

            //reemplazar los params configurados
            if (mensaje.get().getSmsMensajeParams() != null) {
                for (SmsMensajeParam smsMensajeParam : mensaje.get().getSmsMensajeParams()) {
                    logger.info("replace: " + smsMensajeParam.getKeyParam() + " por: " + paramsRequest.get(smsMensajeParam.getKeyParam()));
                    mensajeEnd = mensajeEnd.replaceAll(smsMensajeParam.getKeyParam(), paramsRequest.get(smsMensajeParam.getKeyParam()));
                }
            }
            logger.info("Mensaje a enviar: " + mensajeEnd);
            return mensajeEnd;
        } else {
            return mensajeEnd;
        }
    }

    /**
     * Guardar en la bitacora
     *
     * @param smsBitacora bean de la bitacora a guardar
     */
    private void saveBitacoraSms(SmsBitacora smsBitacora) {
        logger.info("Guardando en la bitacora: " + smsBitacora.toString());
        try {
            smsBitacoraRepository.save(smsBitacora);
        } catch (DataAccessException dataAccessException) {
            logger.error("Ocurrio un error a guardar en la bitacora.", dataAccessException);
        }
    }
}
