package com.addcel.sms.service;

import com.addcel.sms.exception.TwilioClientException;
import com.twilio.rest.api.v2010.account.Message;

public interface TwilioClientService {

    /**
     * Enviar SMS con el api de Twilio
     *
     * @param message     Menssaje a enviar
     * @param phoneNumber numero de telefono a enviar el mensaje
     */
    Message sendSmsTwilio(String message, String phoneNumber) throws TwilioClientException;
}
