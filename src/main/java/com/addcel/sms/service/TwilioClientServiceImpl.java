package com.addcel.sms.service;

import com.addcel.sms.config.Configurations;
import com.addcel.sms.exception.TwilioClientException;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TwilioClientServiceImpl implements TwilioClientService {

    @Autowired
    private Configurations configurations;
    Logger logger = LoggerFactory.getLogger(TwilioClientServiceImpl.class);

    /**
     * Enviar SMS con el api de Twilio
     *
     * @param message     Menssaje a enviar
     * @param phoneNumber numero de telefono a enviar el mensaje
     */
    @Override
    public Message sendSmsTwilio(String message, String phoneNumber) throws TwilioClientException {
        logger.info("exec sendSmsTwilio");
        logger.info("message: " + message);
        logger.info("phoneNumber: " + phoneNumber);
        logger.info("getAccountSidTwilio: " + configurations.getAccountSidTwilio());
        logger.info("getAuthTokenTwilio: " + configurations.getAuthTokenTwilio());

        try {
            Twilio.init(configurations.getAccountSidTwilio(), configurations.getAuthTokenTwilio());
            Message messageSms = Message
                    .creator(new PhoneNumber(phoneNumber), // to
                            new PhoneNumber("+16194042562"), // from
                            message)
                    .create();
            logger.info("Response sms Twilio: " + messageSms.getSid());

            return messageSms;
        } catch (Exception ex) {
            logger.error("Error sendSmsTwilio", ex);
            throw new TwilioClientException();
        }
    }
}
