package com.addcel.sms.util;

public enum CodeNumberCountry {

    MX("1", "+521", "Mexico"),
    CO("2", "+57", "Colombia"),
    USA("3", "+1", "USA"),
    PE("4", "+51", "Peru");

    private String code;
    private String lada;
    private String name;

    CodeNumberCountry(String code, String lada, String name) {
        this.code = code;
        this.lada = lada;
        this.name = name;
    }

    public static CodeNumberCountry getByCode(String code){
        for(CodeNumberCountry codeNumber: CodeNumberCountry.values()){
            if(code.equals(codeNumber.getCode())){
                return codeNumber;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLada() {
        return lada;
    }

    public void setLada(String lada) {
        this.lada = lada;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
