package com.addcel.sms.util;

public interface ConstantSMS {
    String SOURCE = "java";
    String REFERENCE_SMS = "SMS_API";
    String FROM = " MobileCard";

    //Constantes de configuracion
    String USERNAME_CLICKSEND = "USERNAME";
    String PASSWORD_CLICKSEND = "PASSWORD";
    String URL_CLICKSEND = "URL_CLICKSEND";
    String ACCOUNT_SID_TWILIO = "ACCOUNT_SID_TWILIO";
    String AUTH_TOKEN_TWILIO = "AUTH_TOKEN_TWILIO";

    //Status Code
    String RESPONSE_STATUS_CODE_SUCCESS = "SUCCESS";
    String MESSAGE_STATUS_CODE_SUCCES = "SUCCESS";

    String MESSAGE_GENERIC_ERROR = "Ocurrio un error interno en la aplicacion.";

    Integer CODE_ERROR = 0;
    String MENSAJE_ERROR = "ERROR";
    String MENSAJE_EXITO = "EXITO";

    Integer CODE_ERROR_CODE_ACTIVE = -1;
    String MESSAGE_ERROR_CODE_ACTIVE = "Error, el dispositivo ya ha validado su SMS.";

    String MESSAGE_ERROR_NUMBER_EXIST_TUSUARIOS = "Numero telefonico ya esta registrado.";
    Integer CODE_ERROR_CODE_NUMBER_EXIST_TUSUARIOS = -6;

    String MESSAGE_ERROR_CODE_ACTIVE_PHONE_NUMBER = "Numero telefonico previamente validado";
    Integer CODE_ERROR_CODE_ACTIVE_PHONE_NUMBER = 2;

    Integer CODE_ERROR_MAX_CODE_DAY = -2;
    String MESSAGE_ERROR_MAX_CODE_DAY = "Error, Numero maximo de intentos de generacion de codigo.";

    Integer MAX_CODE_BY_DAY = 30;

    Integer CODE_ERROR_SEND_SMS = -4;
    String MESSAGE_ERROR_SEND_SMS = "Ocurrio un error al enviar el mensaje SMS.";

    Integer CODE_ERROR_CODE_NOT_FOUND = -5;

    String MESSAGE_ERROR_CODE_NOT_FOUND = "Error, codigo/numero/imei enviado no es valido.";
}
