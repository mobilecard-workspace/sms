package com.addcel.sms.util;

import java.util.Base64;

public final class SMSUtil {

    /**
     * Generar header Authorization para el API de SendCliend
     *
     * @param username nombre de usuario
     * @param password password
     */
    public final static String getAuthorizationHeader(String username, String password) {
        return Base64.getEncoder().encodeToString(new String(username + ":" + password).getBytes());
    }

    /**
     * Obtener hora en formato unix
     */
    public final static long getUnixTimestamp() {
        return System.currentTimeMillis() / 1000L;
    }
}
