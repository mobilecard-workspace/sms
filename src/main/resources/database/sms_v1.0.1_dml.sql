INSERT INTO mobilecard.SMS_MENSAJES
(ID_MENSAJE, MENSAJE, IDIOMA)
VALUES(2, 'Estimado <nombre>, has recibido un depósito a tu tarjeta MobileCard <cuenta> por la cantidad de $<monto>', 'ESP');

INSERT INTO mobilecard.SMS_MENSAJE_PARAMS
(ID_MENSAJE, KEY_PARAM)
VALUES(2, '<nombre>');

INSERT INTO mobilecard.SMS_MENSAJE_PARAMS
(ID_MENSAJE, KEY_PARAM)
VALUES(2, '<cuenta>');

INSERT INTO mobilecard.SMS_MENSAJE_PARAMS
(ID_MENSAJE, KEY_PARAM)
VALUES(2, '<monto>');
